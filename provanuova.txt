def addUser(request):
    """View to request new login"""
    if request.method == 'POST':
        myForm = AddUserForm(request.POST)
        if myForm.is_valid():
            # set dictionary to fill the template
            values = {'username': myForm.cleaned_data['username'],
                      'name': myForm.cleaned_data['name'],
                      'surname': myForm.cleaned_data['surname'],
                      'email': myForm.cleaned_data['email'],
                      'group': myForm.cleaned_data['group'],
                      'edit': myForm.cleaned_data['insert']}
            message = render_to_string('newuser.txt', values)
            # send message to admins
            mail_admins(_('Aedetrap: new login required'), message)
            if values['group'] is not None:
                #TODO send mail to responsible people of the selected group
                user = ''
            return HttpResponseRedirect('/home/')
    else:
        myForm = AddUserForm()
        return render(request, "search.html", {"form": myForm,
                                               "table": "trap"})
